#include <cstdio>
#include <iostream>
#include <vector>
#include <cuda_runtime.h>

using namespace std;

const int data_size=1<<22;
const int bucket_size=1<<8;
const int n=1<<5;

//////////////////////////////////////////////////////////////////////////
////implementation 0, atomicAdd on global memory
__global__ void Histogram(const int* a,int* b)
{
	unsigned int i=blockIdx.x*blockDim.x+threadIdx.x;
	int v=a[i];
	atomicAdd(&b[v],1);
}

//////////////////////////////////////////////////////////////////////////
////implementation 1: Two atomicAdd on shared memory
__global__ void Histogram_Shared(const int* a,int* c)
{
	__shared__ int bs[bucket_size];
	if(threadIdx.x<bucket_size)bs[threadIdx.x]=0;
	__syncthreads();

	unsigned int i=blockIdx.x*blockDim.x+threadIdx.x;

	for(int j=0;j<n;j++){
		int v1=a[i*n+j];
		atomicAdd(&bs[v1],1);
	}
	__syncthreads();

	if(threadIdx.x<bucket_size)atomicAdd(&c[threadIdx.x],bs[threadIdx.x]);
}

//////////////////////////////////////////////////////////////////////////
////implementation 2
////https://devblogs.nvidia.com/gpu-pro-tip-fast-histograms-using-shared-atomics-maxwell/

__global__ void Histogram_Local(const int* a,int* c)
{
	__shared__ int bs[bucket_size];
	if(threadIdx.x<bucket_size)bs[threadIdx.x]=0;
	__syncthreads();

	unsigned int i=blockIdx.x*blockDim.x+threadIdx.x;

	for(int j=0;j<n;j++){
		int v1=a[i*n+j];
		atomicAdd(&bs[v1],1);
	}
	__syncthreads();

	if(threadIdx.x<bucket_size)c[blockIdx.x*bucket_size+threadIdx.x]=bs[threadIdx.x];
}

__global__ void Histogram_Collect(const int* c,int* b)
{
	unsigned int i=threadIdx.x;
	int x=0;
	for(int j=0;j<blockDim.x;j++){
		x+=c[j*bucket_size+i];
	}
	b[i]=x;
}

int main()
{
	vector<int> a(data_size);
	for(int i=0;i<data_size;i++){
		a[i]=rand()%bucket_size;
	}
	vector<int> b(bucket_size,0);

	int* a_on_dev=0;
	cudaMalloc((void**)&a_on_dev,data_size*sizeof(int));
	cudaMemcpy(a_on_dev,&a[0],sizeof(int)*data_size,cudaMemcpyHostToDevice);
	
	int* b_on_dev=0;
	cudaMalloc((void**)&b_on_dev,bucket_size*sizeof(int));
	cudaMemset(b_on_dev,0,bucket_size*sizeof(int));

	int* c_on_dev=0;
	cudaMalloc((void**)&c_on_dev,n*bucket_size*sizeof(int));
	cudaMemset(c_on_dev,0,n*bucket_size*sizeof(int));


	const int block_dim=1024;
	const int grid_dim=data_size/block_dim;
	
	cudaEvent_t start,end;
	cudaEventCreate(&start);
	cudaEventCreate(&end);
	float gpu_time=0.0f;
	cudaDeviceSynchronize();
	cudaEventRecord(start);

	//Histogram<<<grid_dim,block_dim>>>(a_on_dev,b_on_dev);

	//Histogram_Local<<<grid_dim/n,block_dim>>>(a_on_dev,c_on_dev);
	//Histogram_Collect<<<grid_dim/n,bucket_size>>>(c_on_dev,b_on_dev);

	Histogram_Shared<<<grid_dim/n,block_dim>>>(a_on_dev,b_on_dev);

	cudaMemcpy(&b[0],b_on_dev,bucket_size*sizeof(int),cudaMemcpyDeviceToHost);
	
	cudaEventRecord(end);
	cudaEventSynchronize(end);
	cudaEventElapsedTime(&gpu_time,start,end);
	printf("\nGPU runtime for histogram: %.4f ms\n",gpu_time);
	cudaEventDestroy(start);
	cudaEventDestroy(end);

	cout<<"buckets: "<<endl;
	for(int i=0;i<bucket_size;i++){
		cout<<b[i]<<", ";
	}
	cout<<endl;

	return 0;
}