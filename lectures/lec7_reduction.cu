#include <cstdio>
#include <iostream>
using namespace std;

const unsigned int level_1_size=1<<8;
const unsigned int level_2_size=1<<9;
const unsigned int level_3_size=1<<10;
__device__ int a[level_1_size*level_2_size*level_3_size];
__device__ int b[level_2_size*level_3_size];
__device__ int c[level_3_size];

__global__ void Initialize_A()
{
	int tid=blockIdx.x*blockDim.x+threadIdx.x;
	a[tid]=1;
	__syncthreads();
}

//////////////////////////////////////////////////////////////////////////
////Version 1: First try

__global__ void Reduce1(const int* a_in,int* a_out)
{
	extern __shared__ int data[];
	unsigned int tid=threadIdx.x;							////thread index
	unsigned int idx=blockIdx.x*blockDim.x+threadIdx.x;		////data index
	data[tid]=a_in[idx];

	__syncthreads();

	for(unsigned int s=1;s<blockDim.x;s*=2){
		if(tid%(2*s)==0){
			data[tid]+=data[tid+s];
		}
		__syncthreads();
	}

	if(tid==0){a_out[blockIdx.x]=data[0];}
}

//////////////////////////////////////////////////////////////////////////
////Version 2: Branch convergence

__global__ void Reduce2(const int* a_in,int* a_out)
{
	extern __shared__ int data[];
	unsigned int tid=threadIdx.x;							////thread index
	unsigned int idx=blockIdx.x*blockDim.x+threadIdx.x;		////data index
	data[tid]=a_in[idx];

	__syncthreads();

	for(int s=1;s<blockDim.x;s*=2){
		int i=s*2*tid;
		if(i<blockDim.x){
			data[i]+=data[i+s];
		}
		__syncthreads();
	}

	if(tid==0){a_out[blockIdx.x]=data[0];}
}

//////////////////////////////////////////////////////////////////////////
////Version 3: Avoid bank conflict

__global__ void Reduce3(const int* a_in,int* a_out)
{
	extern __shared__ int data[];
	unsigned int tid=threadIdx.x;							////thread index
	unsigned int idx=blockIdx.x*blockDim.x+threadIdx.x;		////data index
	data[tid]=a_in[idx];

	__syncthreads();
	
	for(unsigned int s=blockDim.x/2;s>0;s/=2){
		if(tid<s){
			data[tid]+=data[tid+s];
		}
		__syncthreads();
	}

	if(tid==0){a_out[blockIdx.x]=data[0];}
}

//////////////////////////////////////////////////////////////////////////
////Version 4: Reduce the number of threads

__global__ void Reduce4(const int* a_in,int* a_out)
{
	extern __shared__ int data[];
	unsigned int tid=threadIdx.x;								////thread index
	unsigned int idx=(blockIdx.x*blockDim.x+threadIdx.x)*2;		////data index
	data[tid]=(a_in[idx]+a_in[idx+1]);	////read multiple data points from the global memory

	__syncthreads();
	
	for(unsigned int s=blockDim.x/2;s>0;s>>=1){
		if(tid<s){
			data[tid]+=data[tid+s];
		}
		__syncthreads();
	}

	if(tid==0){a_out[blockIdx.x]=data[0];}
}

//////////////////////////////////////////////////////////////////////////
////Version 5: loop unrolling

__device__ void ReduceUnroll(volatile int* data, int tid)
{
	data[tid]+=data[tid+32];
	data[tid]+=data[tid+16];
	data[tid]+=data[tid+8];
	data[tid]+=data[tid+4];
	data[tid]+=data[tid+2];
	data[tid]+=data[tid+1];
}

__global__ void Reduce5(const int* a_in,int* a_out)
{
	extern __shared__ int data[];
	unsigned int tid=threadIdx.x;								////thread index
	unsigned int idx=(blockIdx.x*blockDim.x+threadIdx.x)*2;		////data index
	data[tid]=a_in[idx]+a_in[idx+1];

	__syncthreads();
	
	for(unsigned int s=blockDim.x/2;s>32;s>>=1){
		if(tid<s){
			data[tid]+=data[tid+s];
		}
		__syncthreads();
	}

	if(tid<32){ReduceUnroll(data,tid);}
	__syncthreads();

	if(tid==0){a_out[blockIdx.x]=data[0];}
}

//////////////////////////////////////////////////////////////////////////
////Version 6: further loop unrolling with templates

template<unsigned int block_size>
__global__ void Reduce6(const int* a_in,int* a_out)
{
	extern __shared__ int data[];
	unsigned int tid=threadIdx.x;								////thread index
	unsigned int idx=(blockIdx.x*blockDim.x+threadIdx.x)*2;		////data index
	data[tid]=a_in[idx]+a_in[idx+1];

	__syncthreads();
	
	if(block_size>=1024){
		if(tid<512)data[tid]+=data[tid+512];
		__syncthreads();
	}
	if(block_size>=512){
		if(tid<256)data[tid]+=data[tid+256];
		__syncthreads();
	}
	if(block_size>=256){
		if(tid<128)data[tid]+=data[tid+128];
		__syncthreads();
	}
	if(block_size>=128){
		if(tid<64)data[tid]+=data[tid+64];
		__syncthreads();
	}

	if(tid<32){ReduceUnroll(data,tid);}
	__syncthreads();

	if(tid==0){
		a_out[blockIdx.x]=data[0];
	}
}

//////////////////////////////////////////////////////////////////////////
////Lazyman version: Atomic add

__global__ void ReduceAtomicAdd(const int* a_in,int* a_out)
{
	extern __shared__ int data[];
	unsigned int tid=threadIdx.x;
	unsigned int idx=blockIdx.x*blockDim.x+threadIdx.x;

	if(tid==0)data[0]=0;
	__syncthreads();

	atomicAdd(&data[0],a_in[idx]);
	__syncthreads();

	if(tid==0){
		a_out[blockIdx.x]=data[0];
	}
}

int main()
{
	Initialize_A<<<level_1_size*level_2_size,level_3_size>>>();
	int* a_ptr_on_dev=0;
	int* b_ptr_on_dev=0;
	int* c_ptr_on_dev=0;

	cudaGetSymbolAddress((void**)&a_ptr_on_dev,a);
	cudaGetSymbolAddress((void**)&b_ptr_on_dev,b);
	cudaGetSymbolAddress((void**)&c_ptr_on_dev,c);

	{
		cudaEvent_t start,end;
		cudaEventCreate(&start);
		cudaEventCreate(&end);
		float gpu_time=0.0f;
		cudaDeviceSynchronize();
		cudaEventRecord(start);

		Reduce1<<<level_3_size*level_2_size,level_1_size,level_1_size*sizeof(int)>>>(a_ptr_on_dev,b_ptr_on_dev);
		Reduce1<<<level_3_size,level_2_size,level_2_size*sizeof(int)>>>(b_ptr_on_dev,c_ptr_on_dev);
		Reduce1<<<1,level_3_size,level_3_size*sizeof(int)>>>(c_ptr_on_dev,c_ptr_on_dev);

		cudaEventRecord(end);
		cudaEventSynchronize(end);
		cudaEventElapsedTime(&gpu_time,start,end);
		printf("\nGPU runtime for Reduce1: %.4f ms\n",gpu_time);
		cudaEventDestroy(start);
		cudaEventDestroy(end);

		int result=0;
		cudaMemcpy(&result,c_ptr_on_dev,sizeof(int),cudaMemcpyDeviceToHost);
		cout<<"array size: "<<level_1_size*level_2_size*level_3_size<<endl;
		cout<<"result: "<<result<<endl;
	}

	{
		cudaEvent_t start,end;
		cudaEventCreate(&start);
		cudaEventCreate(&end);
		float gpu_time=0.0f;
		cudaDeviceSynchronize();
		cudaEventRecord(start);

		Reduce2<<<level_3_size*level_2_size,level_1_size,level_1_size*sizeof(int)>>>(a_ptr_on_dev,b_ptr_on_dev);
		Reduce2<<<level_3_size,level_2_size,level_2_size*sizeof(int)>>>(b_ptr_on_dev,c_ptr_on_dev);
		Reduce2<<<1,level_3_size,level_3_size*sizeof(int)>>>(c_ptr_on_dev,c_ptr_on_dev);

		cudaEventRecord(end);
		cudaEventSynchronize(end);
		cudaEventElapsedTime(&gpu_time,start,end);
		printf("\nGPU runtime for Reduce2: %.4f ms\n",gpu_time);
		cudaEventDestroy(start);
		cudaEventDestroy(end);

		int result=0;
		cudaMemcpy(&result,c_ptr_on_dev,sizeof(int),cudaMemcpyDeviceToHost);
		cout<<"array size: "<<level_1_size*level_2_size*level_3_size<<endl;
		cout<<"result: "<<result<<endl;
	}

	{
		cudaEvent_t start,end;
		cudaEventCreate(&start);
		cudaEventCreate(&end);
		float gpu_time=0.0f;
		cudaDeviceSynchronize();
		cudaEventRecord(start);

		Reduce3<<<level_3_size*level_2_size,level_1_size,level_1_size*sizeof(int)>>>(a_ptr_on_dev,b_ptr_on_dev);
		Reduce3<<<level_3_size,level_2_size,level_2_size*sizeof(int)>>>(b_ptr_on_dev,c_ptr_on_dev);
		Reduce3<<<1,level_3_size,level_3_size*sizeof(int)>>>(c_ptr_on_dev,c_ptr_on_dev);

		cudaEventRecord(end);
		cudaEventSynchronize(end);
		cudaEventElapsedTime(&gpu_time,start,end);
		printf("\nGPU runtime for Reduce3: %.4f ms\n",gpu_time);
		cudaEventDestroy(start);
		cudaEventDestroy(end);

		int result=0;
		cudaMemcpy(&result,c_ptr_on_dev,sizeof(int),cudaMemcpyDeviceToHost);
		cout<<"array size: "<<level_1_size*level_2_size*level_3_size<<endl;
		cout<<"result: "<<result<<endl;
	}

	{
		cudaEvent_t start,end;
		cudaEventCreate(&start);
		cudaEventCreate(&end);
		float gpu_time=0.0f;
		cudaDeviceSynchronize();
		cudaEventRecord(start);

		Reduce4<<<level_3_size*level_2_size/2,level_1_size,level_1_size*sizeof(int)>>>(a_ptr_on_dev,b_ptr_on_dev);
		Reduce4<<<level_3_size/4,level_2_size,level_2_size*sizeof(int)>>>(b_ptr_on_dev,c_ptr_on_dev);
		Reduce4<<<1,level_3_size/8,level_3_size/8*sizeof(int)>>>(c_ptr_on_dev,c_ptr_on_dev);

		cudaEventRecord(end);
		cudaEventSynchronize(end);
		cudaEventElapsedTime(&gpu_time,start,end);
		printf("\nGPU runtime for Reduce4: %.4f ms\n",gpu_time);
		cudaEventDestroy(start);
		cudaEventDestroy(end);

		int result=0;
		cudaMemcpy(&result,c_ptr_on_dev,sizeof(int),cudaMemcpyDeviceToHost);
		cout<<"array size: "<<level_1_size*level_2_size*level_3_size<<endl;
		cout<<"result: "<<result<<endl;
	}

	{
		cudaEvent_t start,end;
		cudaEventCreate(&start);
		cudaEventCreate(&end);
		float gpu_time=0.0f;
		cudaDeviceSynchronize();
		cudaEventRecord(start);

		////How to set these integer divisors?
		Reduce5<<<level_3_size*level_2_size/2,level_1_size,level_1_size*sizeof(int)>>>(a_ptr_on_dev,b_ptr_on_dev);
		Reduce5<<<level_3_size/4,level_2_size,level_2_size*sizeof(int)>>>(b_ptr_on_dev,c_ptr_on_dev);
		Reduce5<<<1,level_3_size/8,level_3_size/8*sizeof(int)>>>(c_ptr_on_dev,c_ptr_on_dev);

		cudaEventRecord(end);
		cudaEventSynchronize(end);
		cudaEventElapsedTime(&gpu_time,start,end);
		printf("\nGPU runtime for Reduce5: %.4f ms\n",gpu_time);
		cudaEventDestroy(start);
		cudaEventDestroy(end);

		int result=0;
		cudaMemcpy(&result,c_ptr_on_dev,sizeof(int),cudaMemcpyDeviceToHost);
		cout<<"array size: "<<level_1_size*level_2_size*level_3_size<<endl;
		cout<<"result: "<<result<<endl;
	}

	{
		cudaEvent_t start,end;
		cudaEventCreate(&start);
		cudaEventCreate(&end);
		float gpu_time=0.0f;
		cudaDeviceSynchronize();
		cudaEventRecord(start);

		////How to decide the template argument and the divisors?
		Reduce6<256> <<<level_3_size*level_2_size/2,level_1_size,level_1_size*sizeof(int)>>>(a_ptr_on_dev,b_ptr_on_dev);
		Reduce6<512> <<<level_3_size/4,level_2_size,level_2_size*sizeof(int)>>>(b_ptr_on_dev,c_ptr_on_dev);
		Reduce6<128> <<<1,level_3_size/8,level_3_size/8*sizeof(int)>>>(c_ptr_on_dev,c_ptr_on_dev);

		cudaEventRecord(end);
		cudaEventSynchronize(end);
		cudaEventElapsedTime(&gpu_time,start,end);
		printf("\nGPU runtime for Reduce6: %.4f ms\n",gpu_time);
		cudaEventDestroy(start);
		cudaEventDestroy(end);

		int result=0;
		cudaMemcpy(&result,c_ptr_on_dev,sizeof(int),cudaMemcpyDeviceToHost);
		cout<<"array size: "<<level_1_size*level_2_size*level_3_size<<endl;
		cout<<"result: "<<result<<endl;
	}

	{
		cudaEvent_t start,end;
		cudaEventCreate(&start);
		cudaEventCreate(&end);
		float gpu_time=0.0f;
		cudaDeviceSynchronize();
		cudaEventRecord(start);

		ReduceAtomicAdd<<<level_3_size*level_2_size,level_1_size,level_1_size*sizeof(int)>>>(a_ptr_on_dev,b_ptr_on_dev);
		ReduceAtomicAdd<<<level_3_size,level_2_size,level_2_size*sizeof(int)>>>(b_ptr_on_dev,c_ptr_on_dev);
		ReduceAtomicAdd<<<1,level_3_size,level_3_size*sizeof(int)>>>(c_ptr_on_dev,c_ptr_on_dev);

		cudaEventRecord(end);
		cudaEventSynchronize(end);
		cudaEventElapsedTime(&gpu_time,start,end);
		printf("\nGPU runtime for ReduceAtomicAdd: %.4f ms\n",gpu_time);
		cudaEventDestroy(start);
		cudaEventDestroy(end);

		int result=0;
		cudaMemcpy(&result,c_ptr_on_dev,sizeof(int),cudaMemcpyDeviceToHost);
		cout<<"array size: "<<level_1_size*level_2_size*level_3_size<<endl;
		cout<<"result: "<<result<<endl;
	}
}