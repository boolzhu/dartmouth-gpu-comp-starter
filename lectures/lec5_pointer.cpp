#include <iostream>
using namespace std;

////Please read this function carefully to distingush the operator & and *, especially when they are applied to an array
void test_pointer_and_address()
{
	//////value and address
	cout<<"----- a local variable -----"<<endl;
	int x=0;
	cout<<"x:"<<x<<endl;		////value of x
	cout<<"&x:"<<&x<<endl;		////address of x
	cout<<"*&x:"<<*&x<<endl;	////dereference of address of x -> value of x
	//cout<<*(*(&x))<<endl;		////invalid
	cout<<"--------------------"<<endl;

	//cout<<"----- a pointer -----"<<endl;
	int* p=&x;
	cout<<"p:\t\t"<<p<<endl;			////value of a pointer, which is the address it points to
	cout<<"&p:\t\t"<<&p<<endl;		////address of a pointer, which is its own address
	cout<<"*&p:\t\t"<<*&p<<endl;		////dereference of the address of a pointer, which is the value of the pointer
	cout<<"**&p:\t\t"<<**&p<<endl;	////dereference twice of the address of a pointer, which is the value of the variable it points to, think of it as *(*(&p)))
	cout<<"--------------------"<<endl;

	cout<<"----- a pointer of pointer -----"<<endl;
	int** pp=&p;
	cout<<"pp:\t\t"<<pp<<endl;				////value of a (pointer of pointer, "pp"), which is an address
	cout<<"*pp:\t\t"<<*pp<<endl;				////dereference of a pp, which is still an address (the address p is pointing to)
	cout<<"**pp:\t\t"<<**pp<<endl;			////dereference twice of a pp, which is the value of the variable p is pointing to
	cout<<"--------------------"<<endl;

	cout<<"----- an array -----"<<endl;
	int y[2]={1,2};
	cout<<"y:\t\t"<<y<<endl;					////the address of the first element (y[0]) in the array
	cout<<"&y[0]:\t\t"<<&y[0]<<endl;			////the same above
	cout<<"&y:\t\t"<<&y<<endl;				////still the same above. By convention, y=&y for an array
	cout<<"*(&y[0]):\t"<<*(&y[0])<<endl;	////the value of y[0]
	cout<<"*(&y):\t\t"<<*(&y)<<endl;			////(*&y)=y, which is the same as the first one
	cout<<"--------------------"<<endl;

	cout<<"----- a 2D array -----"<<endl;
	int a[2][2]={{0,1},{2,3}};
	cout<<"a:\t\t"<<a<<endl;					////1. the address of the first element (a[0][0]) in the array
	cout<<"a[0]:\t\t"<<a[0]<<endl;			////2. the same above (notice that a[0] is not an element for a 2D array!)
	cout<<"a[0][0]:\t"<<a[0][0]<<endl;		////3. the value of a[0][0]
	cout<<"&a:\t\t"<<&a<<endl;				////4. by convention, &a=a for a 2D array (the same as 1D array)
	cout<<"&a[0]:\t\t"<<&a[0]<<endl;			////5. the same as the first one
	cout<<"&a[0][0]:\t"<<&a[0][0]<<endl;	////6. the same as the first one. Overall, 1,2,4,5,6 are different ways to specify the first address of a 2D array
	cout<<"--------------------"<<endl;

	cout<<"----- a pointer to an array -----"<<endl;
	int* z=new int[2];
	z[0]=101;z[1]=102;
	cout<<"z:\t\t"<<z<<endl;					////the value of the pointer, which is the first address of the array it points to
	cout<<"&z:\t\t"<<&z<<endl;				////the address of the pointer itself, which is different from the address it points to
	cout<<"&z[0]:\t\t"<<&z[0]<<endl;			////the address of the first element of the array the pointer points to
	cout<<"*z:\t\t"<<*z<<endl;				////the value of the first element			
	cout<<"--------------------"<<endl;

	cout<<"----- a pointer of pointer to an array -----"<<endl;
	int** b=new int*[2];
	b[0]=new int[2];
	b[1]=new int[2];
	cout<<"b:\t\t"<<b<<endl;							////these three pointers all point to the address of the first element
	cout<<"b[0]:\t\t"<<b[0]<<endl;
	cout<<"&b[0][0]:\t"<<&b[0][0]<<endl;
	cout<<"--------------------"<<endl;
}

////pointer, pointer of pointer, and reference
////this function will NOT change the value of a outside the function
void pass_pointer_by_value(int* a)		
{
	(*a)=2;
}

////this function will change the value of a outside the function
void pass_pointer_by_reference(int*& a)	
{
	(*a)=3;
}

////this function will change the value of a outside the function. Notice that you are using "**" to dereference the pointer of pointer twice
void pass_pointer_by_pointer(int** a)	
{
	(**a)=4;
}

////this function will not work correctly if the pointer is passed by value instead of by reference or by pointer
void my_new(int* a,size_t size)	////& is the key here!
{
	a=new int[size];
	for(int i=0;i<size;i++){
		a[i]=0;
	}
}

void test_value_reference_pointer()
{
	int b=0;
	pass_pointer_by_value(&b);
	cout<<b<<endl;
	int* p=&b;
	pass_pointer_by_reference(p);
	cout<<b<<endl;
	pass_pointer_by_pointer(&p);	////notice I use an & in front of b, the same as what you need to do with cudaMalloc
	cout<<b<<endl;

	int* a=0;
	my_new(a,10);	////this function needs fix.
	cout<<a[0]<<endl;	
}

////This function tests the various cases of the sizeof operator, especially when it is applied on pointers and arrays
void test_2d_array_sizeof_and_address()
{
	////https://stackoverflow.com/questions/22105797/c-assigning-the-address-of-a-2d-array-to-a-pointer
	//       a    int [10][10]        int (*)[10]      
	//      &a    int (*)[10][10]     
	//      *a    int [10]            int *
	//    a[i]    int [10]            int *
	//   &a[i]    int (*)[10]         
	//   *a[i]    int          
	// a[i][j]    int
	//&a[i][j]    int *

	int a[2][4]={0,1,2,3,4,5,6,7};
	int i=0;int j=0;

	cout<<"address"<<endl;
	cout<<"(a): "		<<(a)<<endl;
	cout<<"(&a): "		<<(&a)<<endl;
	cout<<"(*a): "		<<(*a)<<endl;
	cout<<"(a[i]): "	<<(a[i])<<endl;
	cout<<"(&a[i]): "	<<(&a[i])<<endl;
	cout<<"(*a[i]): "	<<(*a[i])<<endl;
	cout<<"(a[i][j]): "	<<(a[i][j])<<endl;
	cout<<"(&a[i][j]): "<<(&a[i][j])<<endl;

	cout<<"sizeof"<<endl;
	cout<<"sizeof(a): "<<		sizeof(a)<<endl;
	cout<<"sizeof(&a): "<<		sizeof(&a)<<endl;
	cout<<"sizeof(*a): "<<		sizeof(*a)<<endl;
	cout<<"sizeof(a[i]): "<<	sizeof(a[i])<<endl;
	cout<<"sizeof(&a[i]): "<<	sizeof(&a[i])<<endl;
	cout<<"sizeof(*a[i]): "<<	sizeof(*a[i])<<endl;
	cout<<"sizeof(a[i][j]): "<<	sizeof(a[i][j])<<endl;
	cout<<"sizeof(&a[i][j]): "<<sizeof(&a[i][j])<<endl;
}

void test_array_pointer_with_differnt_types()
{
	cout<<"sizeof char: "<<sizeof(char)<<", int: "<<sizeof(int)<<", float: "<<sizeof(float)<<", short: "<<sizeof(short)<<endl;

	//int a[8]={1,2,3,4,5,6,7,8};

	char* mem=(char*)malloc(8*sizeof(int));

	////reinterpret the array as an int array
	int* a=(int*)&mem[0];
	for(int i=0;i<8;i++)a[i]=i;
	cout<<"\na: ";for(int i=0;i<8;i++)cout<<a[i]<<", ";cout<<endl;

	////reinterpret part of the array as another int array
	int* b=&a[2];
	b[0]=1;
	b[1]=2;
	cout<<"b: ";for(int i=0;i<2;i++)cout<<b[i]<<", ";cout<<endl;
	cout<<"\na: ";for(int i=0;i<8;i++)cout<<a[i]<<", ";cout<<endl;

	////reinterpret part of the array as float
	float* c=(float*)&a[4];
	c[0]=1.;
	c[1]=2.;
	c[2]=3.;
	c[3]=4.;
	cout<<"c: ";for(int i=0;i<4;i++)cout<<c[i]<<", ";cout<<endl;
	cout<<"\na: ";for(int i=0;i<8;i++)cout<<a[i]<<", ";cout<<endl;

	////reinterpret part of the array as short
	short* s=(short*)&a[6];
	*s=0;
	*(s+1)=1;
	*(s+2)=2;
	*(s+3)=3;
	cout<<"s: "<<endl;for(int i=0;i<4;i++)cout<<s[i]<<", ";cout<<endl;

	cout<<"\na: ";for(int i=0;i<8;i++)cout<<a[i]<<", ";cout<<endl;
}

void main()
{
	test_pointer_and_address();
	//test_value_reference_pointer();
	//test_2d_array_sizeof_and_address();
	//test_array_pointer_with_differnt_types();
}