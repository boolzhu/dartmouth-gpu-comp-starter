#include <cstdio>

__device__ void cuda_device_function()
{
	printf("This function is called from device only. a=%d, \n", blockIdx.x);
}

__global__ void cuda_global_function()
{
	//cuda_device_function();
	//if(threadIdx.x==0)
	////built-in variables
	////with the type of dim3: 3D vector with the data type of unsigned int

	printf("Hello world from global function, with block %d, thread %d!\n",blockIdx.x,threadIdx.x);
}

__host__ void host_function()
{
	printf("This function is called from host only.\n");
}

int main() {
	host_function();

    //cuda_global_function<<<1,4>>>(); 

	dim3 block_size=dim3(1,2);
	dim3 thread_size=dim3(4,8);
    cuda_global_function<<<block_size,thread_size>>>(); 
	
	return 0;
}