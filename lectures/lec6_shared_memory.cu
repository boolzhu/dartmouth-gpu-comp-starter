#include <cstdio>
#include <iostream>
using namespace std;

const int s=64;
 
////kernel function to use a static shared memory
__global__ void Reverse_Using_Shared_Memory_Static(int* a)
{
	__shared__ int t[s];
	t[threadIdx.x]=a[threadIdx.x];
	__syncthreads();

	a[threadIdx.x]=t[s-1-threadIdx.x];
}

////kernel function to use a dynamic shared memory
__global__ void Reverse_Using_Shared_Memory_Dynamic(int* a)
{
	extern __shared__ int t[];
	t[threadIdx.x]=a[threadIdx.x];
	__syncthreads();
	a[threadIdx.x]=t[s-1-threadIdx.x];
}

////host function to test the two kernels with static and dynamic memory
__host__ void Test_Reverse_Array_Using_Shared_Memory()
{
	int a_host[s];
	for(int i=0;i<s;i++)a_host[i]=i;
	
	cout<<"a before rev: ";for(auto& x:a_host)cout<<x<<", ";cout<<endl;

	int* a_dev=0;
	cudaMalloc((void**)&a_dev,s*sizeof(int));
	cudaMemcpy(a_dev,a_host,s*sizeof(int),cudaMemcpyHostToDevice);
	//Reverse_Using_Shared_Memory_Static<<<1,s>>>(a_dev);
	Reverse_Using_Shared_Memory_Dynamic<<<1,s,s*sizeof(int)>>>(a_dev);
	cudaMemcpy(a_host,a_dev,s*sizeof(int),cudaMemcpyDeviceToHost);

	cout<<"a after rev: ";for(auto& x:a_host)cout<<x<<", ";cout<<endl;
}

//////////////////////////////////////////////////////////////////////////
////kernel functions to use shared memory with multiple array types
__global__ void Reverse_Using_Shared_Memory_Dynamic(int* a,float* b)
{
	extern __shared__ int t[];
	int* ti=(int*)&t[0];
	float* tf=(float*)&t[s];
	ti[threadIdx.x]=a[threadIdx.x];
	tf[threadIdx.x]=b[threadIdx.x];

	__syncthreads();

	a[threadIdx.x]=ti[s-1-threadIdx.x];
	b[threadIdx.x]=tf[s-1-threadIdx.x];
}

////host function to test shared memory with multiple array types
__host__ void Test_Reverse_Two_Arrays_Using_Shared_Memory()
{
	int a_host[s];
	for(int i=0;i<s;i++)a_host[i]=i;
	float b_host[s];
	for(int i=0;i<s;i++)b_host[i]=i;

	cout<<"a before rev: ";for(auto& x:a_host)cout<<x<<", ";cout<<endl;
	cout<<"b before rev: ";for(auto& x:b_host)cout<<x<<", ";cout<<endl;

	int* a_dev=0;
	cudaMalloc((void**)&a_dev,s*sizeof(int));
	cudaMemcpy(a_dev,a_host,s*sizeof(int),cudaMemcpyHostToDevice);
	float* b_dev=0;
	cudaMalloc((void**)&b_dev,s*sizeof(float));
	cudaMemcpy(b_dev,b_host,s*sizeof(float),cudaMemcpyHostToDevice);

	////Two ways to use shared memory with dynamic and static allocation
	//Reverse_Using_Shared_Memory_Static<<<1,s>>>(a_dev);
	Reverse_Using_Shared_Memory_Dynamic<<<1,s,s*sizeof(int)+s*sizeof(float)>>>(a_dev,b_dev);
	cudaMemcpy(a_host,a_dev,s*sizeof(int),cudaMemcpyDeviceToHost);
	cudaMemcpy(b_host,b_dev,s*sizeof(int),cudaMemcpyDeviceToHost);

	cout<<"a after rev: ";for(auto& x:a_host)cout<<x<<", ";cout<<endl;
	cout<<"b after rev: ";for(auto& x:b_host)cout<<x<<", ";cout<<endl;
}

int main()
{
	Test_Reverse_Array_Using_Shared_Memory();
	Test_Reverse_Two_Arrays_Using_Shared_Memory();

	return 0;
}