#include <iostream>
#include <vector>
using namespace std;


__global__ void Init_1(int* x)
{
	unsigned int tid=blockIdx.x*blockDim.x+threadIdx.x;
	int k=0;
	//for(int i=0;i<2;i++)
	//	k+=int(sin(double(tid))+cos(double(k)));
	for(int i=0;i<2;i++){
		k+=tid;
	}
	x[tid]=k;
}

__global__ void Init_2(int* x)
{
	unsigned int tid=blockIdx.x*blockDim.x+threadIdx.x;
	for(int i=0;i<32;i++)
		x[tid]=x[tid]*3;
}

__global__ void Add(const int* a,const int* b,int* c)
{
	unsigned int tid=threadIdx.x;
	for(int i=0;i<32;i++)
		c[tid]=a[tid]+b[tid];
}

void Test_Async_Func_1()
{
	int* a=0,*b=0,*c=0;
	const int size=1024*1024;

	cudaMalloc((void**)&a,size*sizeof(int));
	cudaMalloc((void**)&b,size*sizeof(int));
	cudaMalloc((void**)&c,size*sizeof(int));

	int* h_a=0,*h_b=0,*h_c=0;
	cudaMallocHost((void**)&h_a,size*sizeof(int));
	for(int i=0;i<size;i++)h_a[i]=i;
	cudaMallocHost((void**)&h_b,size*sizeof(int));
	for(int i=0;i<size;i++)h_b[i]=i;
	cudaMallocHost((void**)&h_c,size*sizeof(int));
	for(int i=0;i<size;i++)h_c[i]=0;

	cudaStream_t stream[2];
	cudaStreamCreate(&stream[0]);
	cudaStreamCreate(&stream[1]);

	cudaEvent_t start,end;
	cudaEventCreate(&start);
	cudaEventCreate(&end);
	float gpu_time=0.0f;
	cudaDeviceSynchronize();
	cudaEventRecord(start);

	////asynchronized execution of two streams
	{
		cudaMemcpyAsync(a,&h_a[0],size*sizeof(int),cudaMemcpyHostToDevice,stream[0]);
		Init_1<<<1024,1024,0,stream[0]>>>(a);
		cudaMemcpyAsync(&h_a[0],a,size*sizeof(int),cudaMemcpyDeviceToHost,stream[0]);

		cudaMemcpyAsync(b,&h_b[0],size*sizeof(int),cudaMemcpyHostToDevice,stream[1]);
		Init_2<<<1024,1024,0,stream[1]>>>(b);
		cudaMemcpyAsync(&h_b[0],b,size*sizeof(int),cudaMemcpyDeviceToHost,stream[1]);
	}
	cudaStreamSynchronize(stream[0]);
	cudaStreamSynchronize(stream[1]);

	////synchronized execution of two streams
	//{
	//	cudaMemcpy(a,&h_a[0],size*sizeof(int),cudaMemcpyHostToDevice);
	//	Init_1<<<1024,1024>>>(a);
	//	cudaMemcpy(&h_a[0],a,size*sizeof(int),cudaMemcpyDeviceToHost);

	//	cudaMemcpy(b,&h_b[0],size*sizeof(int),cudaMemcpyHostToDevice);
	//	Init_2<<<1024,1024>>>(b);
	//	cudaMemcpy(&h_b[0],b,size*sizeof(int),cudaMemcpyDeviceToHost);
	//}

	cudaEventRecord(end);
	cudaEventSynchronize(end);
	cudaEventElapsedTime(&gpu_time,start,end);
	printf("\nAsync function call: %.4f ms\n",gpu_time);
	cudaEventDestroy(start);
	cudaEventDestroy(end);

	cudaStreamDestroy(stream[0]);
	cudaStreamDestroy(stream[1]);
}

//////////////////////////////////////////////////////////////////////////
////blocking behavior of default stream

__global__ void Kernel_1()
{
	double s=0;
	for(int i=0;i<32;i++){
		s+=tan(1.)+sin(1.);
	}
}

__global__ void Kernel_2()
{
	double s=0;
	for(int i=0;i<64;i++){
		s+=tan(1.)+sin(1.);
	}
}

__global__ void Kernel_3()
{
	double s=0;
	for(int i=0;i<128;i++){
		s+=tan(1.)+sin(1.);
	}
}

__global__ void Kernel_4()
{
	double s=0;
	for(int i=0;i<64;i++){
		s+=tan(1.f)+sin(1.f);
	}
}

void Test_Async_Func_2()
{

	const int ss=8;
	cudaStream_t stream[ss];
	for(int i=0;i<ss;i++){
		cudaStreamCreate(&stream[i]);
	}

	for(int i=0;i<ss;i++){
		Kernel_1<<<128,1024,0,stream[i]>>>();
		Kernel_2<<<128,1024,0,stream[i]>>>();
		Kernel_3<<<128,512,0,stream[i]>>>();
		Kernel_4<<<512,1024,0,stream[i]>>>();
	}

	cudaDeviceSynchronize();

	for(int i=0;i<ss;i++){
		cudaStreamDestroy(stream[i]);
	}
}

//////////////////////////////////////////////////////////////////////////
////overlapping kernel execution and memory copy
void Test_Async_Func_3()
{
	int* d_a=0;
	const int size=(1<<18);
	cudaMalloc((void**)&d_a,size*sizeof(int));

	int* h_a=0;
	cudaMallocHost((void**)&h_a,size*sizeof(int));
	memset(h_a,0,size*sizeof(int));

	const int ss=8;
	cudaStream_t stream[ss];
	for(int i=0;i<ss;i++){
		cudaStreamCreate(&stream[i]);
	}

	for(int i=0;i<ss;i++){
		size_t offset=i*size/ss;
		cudaMemcpyAsync(d_a+offset,h_a+offset,size/ss*sizeof(int),cudaMemcpyHostToDevice,stream[i]);
		Init_1<<<size/1024,1024,0,stream[i]>>>(d_a+offset);
		cudaMemcpyAsync(h_a+offset,d_a+offset,size/ss*sizeof(int),cudaMemcpyDeviceToHost,stream[i]);
	}
	cudaDeviceSynchronize();

	for(int i=0;i<ss;i++){
		cudaStreamDestroy(stream[i]);
	}
}

//////////////////////////////////////////////////////////////////////////
////inter-stream dependency
void Test_Async_Func_4()
{
	const int ss=8;
	cudaStream_t stream[ss];
	for(int i=0;i<ss;i++){
		cudaStreamCreate(&stream[i]);
	}
	cudaEvent_t event[ss];
	for(int i=0;i<ss;i++){
		cudaEventCreateWithFlags(&event[i],cudaEventDisableTiming);
	}

	for(int i=0;i<ss;i++){
		Kernel_1<<<1,1>>>();
		Kernel_2<<<1,1,0,stream[i]>>>();
		Kernel_3<<<1,1,0,stream[i]>>>();
		Kernel_4<<<1,1,0,stream[i]>>>();
	}

	cudaDeviceSynchronize();

	for(int i=0;i<ss;i++){
		cudaStreamDestroy(stream[i]);
	}
}

//////////////////////////////////////////////////////////////////////////
////stream callback
void CUDART_CB call_back(cudaStream_t stream,cudaError_t status,void* data)
{
	cout<<"callback for stream "<<*((int*)data)<<endl;
}

void Test_Async_Func_5()
{
	const int ss=8;
	cudaStream_t stream[ss];
	for(int i=0;i<ss;i++){
		cudaStreamCreate(&stream[i]);
	}
	cudaEvent_t event[ss];
	for(int i=0;i<ss;i++){
		cudaEventCreateWithFlags(&event[i],cudaEventDisableTiming);
	}

	int id[ss];
	for(int i=0;i<ss;i++){
		id[i]=i;
	}
	for(int i=0;i<ss;i++){
		Kernel_1<<<1,1,0,stream[i]>>>();
		Kernel_2<<<1,1,0,stream[i]>>>();
		Kernel_3<<<1,1,0,stream[i]>>>();
		Kernel_4<<<1,1,0,stream[i]>>>();
		cudaStreamAddCallback(stream[i],call_back,(void*)&id[i],0);
	}

	cudaDeviceSynchronize();

	for(int i=0;i<ss;i++){
		cudaStreamDestroy(stream[i]);
	}
}

//////////////////////////////////////////////////////////////////////////
////CPU and GPU concurrency
void Test_Async_Func_6()
{
	int* a=0;
	const int size=1024*1024;

	cudaMalloc((void**)&a,size*sizeof(int));

	int* h_a=0;
	cudaMallocHost((void**)&h_a,size*sizeof(int));
	for(int i=0;i<size;i++)h_a[i]=i;

	int counter=0;

	cudaEvent_t start,end;
	cudaEventCreate(&start);
	cudaEventCreate(&end);
	float gpu_time=0.0f;

	cudaDeviceSynchronize();
	cudaEventRecord(start);
	cudaMemcpy(a,&h_a[0],size*sizeof(int),cudaMemcpyHostToDevice);
	Init_1<<<1024,1024>>>(a);
	cudaMemcpy(&h_a[0],a,size*sizeof(int),cudaMemcpyDeviceToHost);
	cudaEventRecord(end);
	cudaEventSynchronize(end);

	while(cudaEventQuery(end)==cudaErrorNotReady){
		counter++;
	}

	cudaEventElapsedTime(&gpu_time,start,end);
	printf("\ncounter=%d\n",counter);
	printf("\nAsync function call: %.4f ms\n",gpu_time);
	cudaEventDestroy(start);
	cudaEventDestroy(end);
}

//////////////////////////////////////////////////////////////////////////
////thread fence
__device__ volatile int x=1,y=2;

__global__ void Kernel()
{
	if(threadIdx.x==0){
		x=10;
		y=20;
	}
	if(threadIdx.x==1){
		int a=x;
		int b=y;
		printf("a=%d, b=%d",a,b);
	}
}

void Test_Async_Func_7()
{
	Kernel<<<1,128>>>();
}

__device__ volatile int is_s_changed=0;
__device__ volatile int s=-1;

__global__ void Kernel_A()
{
	s=threadIdx.x;
	__threadfence();
	//atomicAdd(&is_s_changed,1);
}

__global__ void Kernel_B()
{
	int a=-2;
	while(is_s_changed){
		a=s;
		break;
	}
	printf("a=%d",a);
}

void Test_Async_Func_8()
{
	Kernel_A<<<1,1>>>();
	Kernel_B<<<1,1>>>();
}

//////////////////////////////////////////////////////////////////////////
////More sync threads

__global__ void Kernel_Count()
{
	int a=threadIdx.x%2;
	int count=__syncthreads_count(a);
	if(threadIdx.x==0)
		printf("count=%d",count);
}

__global__ void Kernel_And()
{
	int a=threadIdx.x;
	int and=__syncthreads_and(a);
	if(threadIdx.x==0)
		printf("and=%d",and);
}

__global__ void Kernel_Or()
{
	int a=0;
	int or=__syncthreads_or(a);
	if(threadIdx.x==0)
		printf("or=%d",or);
}

void Test_Async_Func_9()
{
	Kernel_Or<<<1,16>>>();
}

//////////////////////////////////////////////////////////////////////////
////Sync warp

__global__ void Kernel_Warp()
{
	for(int i=0;i<20000;i++){
		if(i==5000){
			__syncwarp(0xffff0000);
		}
		printf("thread %d, i=%d",threadIdx.x,i);
	}
}

int main()
{
	Test_Async_Func_1();
	//Test_Async_Func_2();
	//Test_Async_Func_3();
	//Test_Async_Func_4();
	//Test_Async_Func_5();
	//Test_Async_Func_6();
	//Test_Async_Func_7();
	//Test_Async_Func_8();

	return 0;
}