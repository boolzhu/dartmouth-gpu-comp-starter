//////////////////////////////////////////////////////////////////////////
////This is the code implementation for GPU Premier League Round 1
//////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include <cuda_runtime.h>

using namespace std;

////This is a matrix class to carry out linear algebra operations on both GPU and CPU
////It is the same as the sample code I showed in class on Week 3. 

class Matrix{
public:
    int m=0;							////number of rows
    int n=0;							////number of columns
	vector<float> elements_on_host;		////we use a std::vector for the element array on host
    float* elements_on_dev=0;			////we use a pointer for the element array on device
	bool on_host=true;

	////constructors
	__host__ Matrix(){}

	__host__ Matrix(const int _m,const int _n,bool _on_host=true)
	{
		on_host=_on_host;
		if(on_host)Resize_On_Host(_m,_n);
		else Resize_On_Device(_m,_n);
	}

	////destructor
	__host__ ~Matrix()
	{
		if(!on_host&&elements_on_dev!=0) cudaFree(elements_on_dev);		
	}

	////Resize on host or device
	__host__ void Resize_On_Host(const int _m,const int _n)
	{
		if(m==_m&&n==_n)return;
		m=_m;
		n=_n;
		elements_on_host.resize(m*n);
	}

	__host__ void Resize_On_Device(const int _m,const int _n)
	{
		if(m==_m&&n==_n)return;
		m=_m;
		n=_n;
		if(elements_on_dev!=0)cudaFree(elements_on_dev);
		cudaMalloc((void**)&elements_on_dev,m*n*sizeof(float));
	}

	////random access a matrix element
	inline __host__ float& operator() (const int i,const int j)
	{
		return elements_on_host[i*n+j];
	}

	inline __host__ const float& operator() (const int i,const int j) const
	{
		return elements_on_host[i*n+j];
	}

	////copy data with four cases (CPU->CPU, GPU->CPU, GPU->GPU, CPU->GPU)
	__host__ Matrix& operator= (const Matrix& mtx)
	{
		if(on_host&&mtx.on_host){
			Resize_On_Host(mtx.m,mtx.n);
			elements_on_host=mtx.elements_on_host;
		}
		else if(on_host&&!mtx.on_host){
			Resize_On_Host(mtx.m,mtx.n);
			cudaMemcpy(&elements_on_host[0],mtx.elements_on_dev,m*n*sizeof(float),cudaMemcpyDeviceToHost);
		}
		else if(!on_host&&!mtx.on_host){
			Resize_On_Device(mtx.m,mtx.n);
			cudaMemcpy(elements_on_dev,mtx.elements_on_dev,mtx.m*n*sizeof(float),cudaMemcpyDeviceToDevice);
		}
		else if(!on_host&&mtx.on_host){
			Resize_On_Device(mtx.m,mtx.n);
			cudaMemcpy(elements_on_dev,&mtx.elements_on_host[0],m*n*sizeof(float),cudaMemcpyHostToDevice);
		}
		return *this;
	}

	////print matrix elements on screen
	__host__ friend ostream & operator << (ostream &out,const Matrix &mtx)
	{
		if(!mtx.on_host)
			cout<<"Print for matrix on device is not supported."<<endl;

		for(int i=0;i<mtx.m;i++){
			for(int j=0;j<mtx.n;j++){
				out<<mtx(i,j)<<", ";
			}
			out<<std::endl;
		}
		return out;
	}
};

__global__ void Transpose(const float* Ae,float* ATe,const int Am,const int An)
{
	int i=blockIdx.x*blockDim.x+threadIdx.x;
	int j=blockIdx.y*blockDim.y+threadIdx.y;

	////AT(j,i)=A(i,j)
	ATe[i*Am+j]=Ae[i*An+j];
} 

////This is the fast version implementing the coalesced read from global memmory
__global__ void Transpose2(const float* Ae,float* ATe,const int Am,const int An)
{
	__shared__ int s[16][17];

	////block (bx,by) in A
	int j=blockIdx.x*blockDim.x+threadIdx.x;	////x for column
	int i=blockIdx.y*blockDim.y+threadIdx.y;	////y for row
	s[threadIdx.y][threadIdx.x]=Ae[i*An+j];
	__syncthreads();

	////block (by,bx) in AT
	int tj=blockIdx.y*blockDim.x+threadIdx.x;	////x for column
	int ti=blockIdx.x*blockDim.y+threadIdx.y;	////y for row
	ATe[ti*Am+tj]=s[threadIdx.x][threadIdx.y];
} 

////Here are the test functions for your three kernel implementations

ofstream out;

__host__ void Test_Transpose_On_GPU(const Matrix& A,Matrix& AT)
{
	//// Load A to device memory
	Matrix A_on_dev(A.m,A.n,false);
	A_on_dev=A;

	//// Allocate AT in device memory
	Matrix AT_on_dev(A_on_dev.n,A_on_dev.m,false);

	cudaEvent_t start,end;
	cudaEventCreate(&start);
	cudaEventCreate(&end);
	float gpu_time=0.0f;
	cudaDeviceSynchronize();
	cudaEventRecord(start);

	//// Invoke kernel
	const int block_size=16;
	const int block_num_x=A.n/block_size;
	const int block_num_y=A.m/block_size;

	Transpose<<<dim3(block_num_x,block_num_y),dim3(block_size,block_size)>>>
		(A_on_dev.elements_on_dev,AT_on_dev.elements_on_dev,A_on_dev.m,A_on_dev.n);

	cudaEventRecord(end);
	cudaEventSynchronize(end);
	cudaEventElapsedTime(&gpu_time,start,end);
	printf("\nGPU runtime for transpose: %.4f ms\n",gpu_time);
	cudaEventDestroy(start);
	cudaEventDestroy(end);

	//// Transfer data back to CPU
	AT=AT_on_dev;

	//cout<<A<<endl;
	//cout<<AT<<endl;
}

int main()
{
	const int m=16;
	const int n=16;

	Matrix h_A(m,n);
	for(int i=0;i<m;i++){
		for(int j=0;j<n;j++){
			h_A(i,j)=(float)(i*n+j);
		}
	}

	Matrix h_AT(n,m);
	for(int i=0;i<n;i++){
		for(int j=0;j<m;j++){
			h_AT(i,j)=1.f;
		}
	}

	Test_Transpose_On_GPU(h_A,h_AT);

	return 0;
}