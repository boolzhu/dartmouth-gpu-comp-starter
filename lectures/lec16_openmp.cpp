#include <cstdio>
#include <cmath>
#include <chrono>
#include <iostream>
#include <omp.h>
using namespace std;

//int main()
//{
//	const int size = 256;
//	double sinTable[size];
//    
//	auto start=chrono::system_clock::now();
//
//	#pragma omp simd
//	for(int n=0; n<size; ++n)
//		sinTable[n] = std::sin(2 * 3.1415926 * n / size);
//  
//	auto end=chrono::system_clock::now();
//	chrono::duration<double> t=end-start;
//	cout<<"run time: "<<t.count()*1000.<<" ms."<<endl;
//}

void Hello_World()
{
	#pragma omp parallel num_threads(4)
	{
		printf("Hello, world.\n");
	}
}

void Atomic()
{
	int a=0;

	#pragma omp parallel for
	for(int i=0;i<96;i++){
		#pragma omp atomic
		a+=i;
	}
	#pragma omp barrier

	cout<<"a="<<a<<endl;
}

void Critical()
{
	int a=0;

	#pragma omp parallel for
	for(int i=0;i<96;i++){
		#pragma omp critical
		{
			a+=i;
			a+=i;
		}
	}
	#pragma omp barrier

	cout<<"a="<<a<<endl;
}

void Reduction()
{
	int a=0;

	#pragma omp parallel for reduction (+:a)
	for(int i=0;i<96;i++){
		a+=i;
	}
	#pragma omp barrier

	cout<<"a="<<a<<endl;	
}

////three types of work-sharing construct: for, 
////for-loop
void For()
{
	#pragma omp parallel 
	{
		#pragma omp for 
		for(int i=0;i<96;i++){
			int tid=omp_get_thread_num();
			printf("index %d, tid %d\n",i,tid);
		}
	}

}

////sections
void Sections()
{
	#pragma omp /*parallel*/ sections
	{
		#pragma omp section
		{
			for(int i=0;i<8;i++){
				printf("A %d\n",i);
			}
		}

		#pragma omp section
		{
			for(int i=0;i<8;i++){
				printf("B %d\n",i);
			}
		}

		#pragma omp section
		{
			for(int i=0;i<8;i++){
				printf("C %d\n",i);
			}
		}

		#pragma omp section
		{
			for(int i=0;i<8;i++){
				printf("D %d\n",i);
			}
		}
	}
}

void Single()
{
	int a=-1;

	#pragma omp parallel shared(a)
	{
		#pragma omp master
		{
			a=omp_get_thread_num();
		}
		#pragma omp barrier	////master has no implicit barrier

		#pragma omp for
		for(int i=0;i<8;i++){
			printf("a=%d\n",a);		
		}
	}
}

void SIMD()
{
	#pragma omp simd
	for(int i=0;i<96;i++){
		int tid=omp_get_thread_num();
		printf("index %d, tid %d\n",i,tid);
	}
}

////thread ID
void Private_Var()
{
	int tid;

	#pragma omp parallel //shared (tid)
	{
		tid=omp_get_thread_num();
		printf("tid =%d\n",tid);
	}
}

////thread number
////sync threads
////local var
void Shared_Var()
{
	int tid;
	int tn;
	int tmax;

	tid=omp_get_thread_num();
	std::cout<<"default tid="<<tid<<endl;

	#pragma omp parallel private (tid)
	{
		tid=omp_get_thread_num();
		////int tid=omp_get_thread_num();

		#pragma omp barrier
		if(tid==0){
			tn=omp_get_num_threads();
			tmax=omp_get_max_threads();
			printf("tn=%d\n",tn);
			printf("tmax=%d\n",tmax);
		}
	}
}

////shared var
void Shared_Var2()
{
	int a[16]={0};
	int b[16]={0};
	int tid;

	#pragma omp parallel shared (a,b) private(tid)
	{
		tid=omp_get_thread_num();
		b[tid]=a[tid]+tid;
	}

	for(int i=0;i<16;i++)cout<<a[i]<<", ";cout<<endl;
	for(int i=0;i<16;i++)cout<<b[i]<<", ";cout<<endl;
}

////global to local var communication
////firstprivate
void Shared_Var3()
{
	int a=1;

	#pragma omp parallel firstprivate(a)
	{
		int tid=omp_get_thread_num();
		a+=tid;
		printf("local a=%d\n",a);
	}

	cout<<"global a="<<a<<endl;
}

////local to global var communication
////lastprivate
void Shared_Var4()
{
	int a=1;

	#pragma omp parallel for lastprivate(a)
	for(int i=0;i<12;i++)
	{
		int tid=omp_get_thread_num();
		a+=tid;
		printf("local a=%d\n",a);
	}

	cout<<"global a="<<a<<endl;
}

////global var for threads
////threadprivate
int a=10;
#pragma omp threadprivate(a)

void Shared_Var5()
{
	#pragma omp parallel
	{
		int tid=omp_get_thread_num();
		a+=(tid+1);
		printf("tid %d, a: %d\n",tid,a);
	}

	cout<<"global a="<<a<<endl;

	#pragma omp parallel //copyin(a)
	{
		int tid=omp_get_thread_num();
		a+=(tid+2);
		printf("tid %d, a: %d\n",tid,a);
	}

	cout<<"global a="<<a<<endl;
}

void Parallel_If()
{
	bool use_parallel=false;
	#pragma omp parallel for if(use_parallel)
	for(int i=0;i<8;i++){
		printf("tid %d\n",i);
	}
}

void Ordered()
{
	#pragma omp parallel for ordered
	for(int i=0;i<16;i++){
		printf("unordered %d\n",i);
		//#pragma omp barrier

		#pragma omp ordered
		{
			printf("ordered %d\n",i);
		}
	}
}

void Nested_For()
{
	#pragma omp parallel for collapse(2)
	for(int i=0;i<4;i++){
		for(int j=0;j<8;j++){
			printf("i=%d,j=%d\n",i,j);
		}
	}
}

////nowait removes the implicit barrier within the work-sharing constructs
void No_Wait()
{
	#pragma omp parallel 
	{
		#pragma omp for nowait
		for(int i=0;i<8;i++){
			printf("A: %d\n",i);
		}

		#pragma omp for
		for(int i=0;i<8;i++){
			printf("B: %d\n",i);
		}
	}
}

void Lock() {
    omp_lock_t lock;
    omp_init_lock(&lock);
	
	#pragma omp parallel for
    for(int i = 0; i < 8; i++) {
        omp_set_lock(&lock);
        printf("Thread %d: +\n", omp_get_thread_num());
        system("sleep 1.0");
        printf("Thread %d: -\n", omp_get_thread_num());
        omp_unset_lock(&lock);
    }

    omp_destroy_lock(&lock);
}

int main(void)
{
	//Hello_World();
	//Atomic();
	//Critical();
	//Reduction();

	//For();
	//Sections();
	//Single();
	//SIMD();

	//Private_Var();
	//Shared_Var();
	//Shared_Var2();
	//Shared_Var3();
	//Shared_Var4();
	//Shared_Var5();

	//Parallel_If();
	//Ordered();
	//Nested_For();
	//No_Wait();
	Lock();

	return 0;
}