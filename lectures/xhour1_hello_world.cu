#include <cuda.h>
#include <stdio.h>


__global__ void cuda_hello(){
    printf("Hello from block %d, thread %d\n", blockIdx.x, threadIdx.x);
}

int main() {
    cuda_hello<<<5,5>>>(); 
    cudaDeviceSynchronize();
    return 0;
}