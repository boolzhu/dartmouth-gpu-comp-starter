#include <cstdio>
#include <vector>
#include <iostream>
using namespace std;

const int sx=64;
__device__ int x_on_dev[sx];
__device__ int z_on_dev[sx];

__global__ void vec_add(int* a,int* b)
{
	int i=threadIdx.x;
	a[i]+=b[i];
}

////manipulate on a passed-in array
__global__ void vec_self_add(int* a)
{
	a[threadIdx.x]+=1;
}

////manipulate on the global variable
__global__ void vec_self_add_on_x()
{
	x_on_dev[threadIdx.x]+=1;
}

//////////////////////////////////////////////////////////////////////////
////data transfer for dynamic arrays
__host__ void Test_Host_To_Device_To_Host_Dynamic()
{
	////allocate and initialize arrays on host
	int n=64;
	int* host_a=(int*)malloc(n*sizeof(int));
	int* host_b=(int*)malloc(n*sizeof(int));
	for(int i=0;i<n;i++){
		host_a[i]=0;
		host_b[i]=i;
	}

	////allocate arrays on device
	int* dev_a=nullptr;
	cudaMalloc((void**)&dev_a,n*sizeof(int));

	int* dev_b=nullptr;
	cudaMalloc((void**)&dev_b,n*sizeof(int));

	////copy from host to device
	cudaMemcpy(dev_a,host_a,n*sizeof(int),cudaMemcpyHostToDevice);
	cudaMemcpy(dev_b,host_b,n*sizeof(int),cudaMemcpyHostToDevice);
	
	////operate
	vec_add<<<1,n>>>(dev_a,dev_b);

	////copy from device to host
	cudaMemcpy(host_a,dev_a,n*sizeof(int),cudaMemcpyDeviceToHost);

	////print out results on host
	printf("add results\n");
	for(int i=0;i<n;i++){
		printf("%d, ",host_a[i]);
	}

	////free memory allocated
	cudaFree(dev_a);
	cudaFree(dev_b);
	free(host_a);
	free(host_b);
}

//////////////////////////////////////////////////////////////////////////
////data transfer for static arrays
__host__ void Test_Host_To_Device_To_Host_Static()
{
	////initialize a vector on host
	vector<int> y_on_host(sx);
	for(int i=0;i<sx;i++)y_on_host[i]=i;

	printf("\ny on host before add:\n");
	for(int i=0;i<sx;i++)printf("%d, ",y_on_host[i]);printf("\n");

	//////////////////////////////////////////////////////////////////////////
	////There are two ways to transfer data between a host variable and a device variable in static memory (e.g., x_on_dev, which is declared as a global __device__ variable here)
	//////////////////////////////////////////////////////////////////////////

	//////Option 1: get the address of the static variable and treat it as a variable in dynamic memory 
	//int* x_on_dev_ptr=nullptr;
	//cudaGetSymbolAddress((void**)&x_on_dev_ptr,x_on_dev);
	//cudaMemcpy(x_on_dev_ptr,&y_on_host[0],sx*sizeof(int),cudaMemcpyHostToDevice);
	//vec_self_add<<<1,sx>>>(x_on_dev_ptr);
	//cudaMemcpy(&y_on_host[0],x_on_dev_ptr,sx*sizeof(int),cudaMemcpyDeviceToHost);

	//////Option 2: use cudaMemcpyToSymbol and cudaMemcpyFromSymbol
	//cudaMemcpyToSymbol(x_on_dev,&y_on_host[0],sx*sizeof(int));
	//vec_self_add_on_x<<<1,sx>>>();
	//cudaMemcpyFromSymbol(&y_on_host[0],x_on_dev,sx*sizeof(int));

	////We can also transfer data between static arrays that are both on device
	int* x_on_dev_ptr=nullptr;
	cudaGetSymbolAddress((void**)&x_on_dev_ptr,x_on_dev);
	cudaMemcpyToSymbol(x_on_dev,&y_on_host[0],sx*sizeof(int));
	vec_self_add_on_x<<<1,sx>>>();
	cudaMemcpyToSymbol(z_on_dev,x_on_dev_ptr,sx*sizeof(int),0,cudaMemcpyDeviceToDevice);
	cudaMemcpyFromSymbol(&y_on_host[0],z_on_dev,sx*sizeof(int));

	printf("\ny on host after add:\n");
	for(int i=0;i<sx;i++)printf("%d, ",y_on_host[i]);printf("\n");
}

//////////////////////////////////////////////////////////////////////////
////constant memory
__constant__ float a[8];

__global__ void Use_Constant_Array(float* c)
{
	//a[0]=1;	////Cannot modify the value of a __constant__ variable
	for(int i=0;i<8;i++){
		c[threadIdx.x]+=a[i];
	}
}

__host__ void Test_Constant()
{
	float b[8]={1,2,3,4,5,6,7,8};
	cudaMemcpyToSymbol(a,b,8*sizeof(float));
	float* c=0;
	cudaMalloc((void**)&c,8*sizeof(float));
	cudaMemset((void**)&c,0,8*sizeof(float));
	Use_Constant_Array<<<1,8>>>(c);
	cudaMemcpy(b,c,8*sizeof(float),cudaMemcpyDeviceToHost);
	cout<<"Values of b:\n";
	for(int i=0;i<8;i++){
		cout<<b[i]<<", ";
	}
	cout<<endl;
}

int main()
{
	Test_Host_To_Device_To_Host_Dynamic();
	Test_Host_To_Device_To_Host_Static();
	Test_Constant();

	return 0;
}