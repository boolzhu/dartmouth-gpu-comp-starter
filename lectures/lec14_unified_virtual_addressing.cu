#include <cstdio>
#include <iostream>
using namespace std;

__device__ __managed__ int a[8];

__global__ void Kernel()
{
	int flag=__isGlobal(a);
	printf("\nisGlobal: %d\n",flag);
}

int main()
{
	int* b=new int[8];
	int* c;
	cudaMalloc((void**)&c,8*sizeof(int));

	int* d;
	cudaMallocManaged((void**)&d,8*sizeof(int));

	Kernel<<<1,1>>>();

	{
		cudaPointerAttributes ptr_att;
		cudaPointerGetAttributes(&ptr_att,a);
		cout<<"device: "<<ptr_att.device<<endl;
		cout<<"isManaged: "<<ptr_att.isManaged<<endl;
		cout<<"memoryType: "<<ptr_att.memoryType<<endl;
	}

	{
		cudaPointerAttributes ptr_att;
		cudaPointerGetAttributes(&ptr_att,b);
		cout<<"device: "<<ptr_att.device<<endl;
		cout<<"isManaged: "<<ptr_att.isManaged<<endl;
		cout<<"memoryType: "<<ptr_att.memoryType<<endl;
	}
	
	{
		cudaPointerAttributes ptr_att;
		cudaPointerGetAttributes(&ptr_att,c);
		cout<<"device: "<<ptr_att.device<<endl;
		cout<<"isManaged: "<<ptr_att.isManaged<<endl;
		cout<<"memoryType: "<<ptr_att.memoryType<<endl;
	}

	{
		cudaPointerAttributes ptr_att;
		cudaPointerGetAttributes(&ptr_att,d);
		cout<<"device: "<<ptr_att.device<<endl;
		cout<<"isManaged: "<<ptr_att.isManaged<<endl;
		cout<<"memoryType: "<<ptr_att.memoryType<<endl;
	}
}