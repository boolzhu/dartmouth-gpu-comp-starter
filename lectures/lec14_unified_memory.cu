#include <cstdio>
#include <iostream>
using namespace std;

const int s=32;

__global__ void Kernel(int* a)
{
	a[threadIdx.x]+=threadIdx.x;
}

__device__ __managed__ int b[s];

int main()
{
	
	int* a;
	cudaMallocManaged((void**)&a,s*sizeof(int));
	for(int i=0;i<s;i++)
		a[i]=1;

	Kernel<<<1,s>>>(a);
	cudaDeviceSynchronize();

	cout<<"a: "<<endl;
	for(int i=0;i<s;i++)
		cout<<a[i]<<", ";

	for(int i=0;i<s;i++)
		b[i]=i;

	Kernel<<<1,s>>>(b);
	cudaDeviceSynchronize();

	cout<<"b: "<<endl;
	for(int i=0;i<s;i++)
		cout<<b[i]<<" ";
	
	return 0;
}