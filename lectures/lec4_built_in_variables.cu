#include <cstdio>

__host__ void Check_Type_Size()
{
	////print out the size of eath data type
	printf("char: \t%zu bytes\n",sizeof(char));
	printf("short: \t%zu bytes\n",sizeof(short));
	printf("int: \t%zu bytes\n",sizeof(int));
	printf("long: \t%zu bytes\n",sizeof(long));
	printf("long long: \t%zu bytes\n",sizeof(long long));
	printf("float: \t%zu bytes\n",sizeof(float));
	printf("double:\t%zu bytes\n",sizeof(double));
	
	printf("int1: \t%zu bytes\n",sizeof(int1));
	printf("int2: \t%zu bytes\n",sizeof(int2));
	printf("int3: \t%zu bytes\n",sizeof(int3));
	printf("int4: \t%zu bytes\n",sizeof(int4));
	
	printf("dim3: \t%zu bytes\n",sizeof(dim3));

	////CUDA also has types of char1-4,uint1-4,short1-4,ushort1-4,
	////long1-4,longlong1-4,ulonglong1-4,float1-4,double1-4
}

__host__ void Check_Built_In_Types()
{
	////constructor for built-in types: make_<type>d(...)
	int3 a=make_int3(1,2,3);
	float2 b=make_float2(1.f,2.f);

	////dim3 can take 1 or 2 or 3 arguments
	dim3 c=dim3(1);
	dim3 d=dim3(1,2);
	dim3 e=dim3(1,2,3);
	dim3 f=4;
}

__global__ void Check_Built_In_Variables()
{
	////print out all the built-in variables
	printf("gridDim:%d,%d,%d\n",gridDim.x,gridDim.y,gridDim.z);
	printf("blockIdx:%d,%d,%d\n",blockIdx.x,blockIdx.y,blockIdx.z);
	printf("blockDim:%d,%d,%d\n",blockDim.x,blockDim.y,blockDim.z);
	printf("threadIdx:%d,%d,%d\n",threadIdx.x,threadIdx.y,threadIdx.z);
	printf("warpSize:%d\n",warpSize);
}

int main()
{
	Check_Type_Size();
	Check_Built_In_Types();

	dim3 block_size=dim3(1,2);
	dim3 thread_size=dim3(4);
	Check_Built_In_Variables<<<block_size,thread_size>>>();

	return 0;
}