#include <thrust/version.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/fill.h>
#include <thrust/sequence.h>
#include <thrust/transform.h>
#include <thrust/replace.h>
#include <thrust/functional.h>
#include <thrust/sort.h>
#include <thrust/extrema.h>
#include <thrust/inner_product.h>
#include <thrust/iterator/constant_iterator.h>
#include <iostream>

void Thrust_Version()
{
	int major=THRUST_MAJOR_VERSION;
	int minor=THRUST_MINOR_VERSION;
	std::cout<<"Thrust v"<<major<<"."<<minor<<std::endl;
}

void Print(const thrust::device_vector<int>& d)
{
	std::cout<<"device_vector: ";
	for(const auto& x:d)
		std::cout<<x<<", ";
	std::cout<<std::endl;
}

struct my_func
{
	int a=0;
	my_func(int _a):a(_a){}

	__host__ __device__ int operator()(const int& x,const int& y) const
	{
		return x+a*y;
	}
};

void Thrust_Vectors()
{
	////std vector
	std::vector<int> v={5,6,7,8};

	////thrust host vector
	thrust::host_vector<int> h(4);
	for(int i=0;i<h.size();i++)
		h[i]=i;
	
	std::cout<<"host_vector size: "<<h.size()<<std::endl;
	for(auto& x:h)
		std::cout<<x<<", ";
	std::cout<<std::endl;
	
	////thrust device vector
	thrust::device_vector<int> d;
	
	////copy from std::vector
	d=v;
	Print(d);

	////copy from host
	d=h;
	Print(d);

	////assign values to device from host
	d[0]=9;
	Print(d);

	////assign 10 to d
	thrust::fill(d.begin(),d.end(),10);
	Print(d);

	////assign 0,1,2,3,... to d
	thrust::sequence(d.begin(),d.end());
	Print(d);
	
	////copy from v to d
	thrust::copy(v.begin(),v.begin()+3,d.begin());
	Print(d);

	////device_ptr and device_malloc
	thrust::device_ptr<int> dev_ptr=thrust::device_malloc<int>(4);
	dev_ptr[0]=101;
	dev_ptr[1]=102;
	dev_ptr[2]=103;
	dev_ptr[3]=104;
	for(int i=0;i<4;i++)std::cout<<dev_ptr[i]<<", ";std::cout<<std::endl;

	thrust::copy(dev_ptr,dev_ptr+3,d.begin());
	Print(d);

	////device ptr to raw ptr
	int* raw_ptr=thrust::raw_pointer_cast(dev_ptr);
	//std::cout<<raw_ptr[0]<<std::endl;	////cannot call this!

	////raw ptr to device ptr
	int* raw_ptr2;
	cudaMalloc((void**)&raw_ptr2,4*sizeof(int));
	thrust::device_ptr<int> dev_ptr2(raw_ptr2);
	thrust::fill(dev_ptr2,dev_ptr2+4,1);
	std::cout<<"dev_ptr2: "<<dev_ptr2[0]<<std::endl;	////print device_ptr on screen

	////transform
	thrust::device_vector<int> d1(4,1),d2(4,2),d3(4,3);
	////d2=-d1
	thrust::transform(/*src*/d1.begin(),d1.end(),/*dst*/d2.begin(),thrust::negate<int>());
	Print(d2);

	////replace
	thrust::replace(d1.begin(),d1.end(),1,10);
	Print(d1);

	////customized transform, d2=d1+a*d2
	thrust::transform(/*src1*/d1.begin(),d1.end(),/*src2*/d2.begin(),/*des*/d2.begin(),my_func(4));

	////reduction
	int sum=thrust::reduce(d1.begin(),d1.end(),(int)0,thrust::plus<int>());
	std::cout<<"sum: "<<sum<<std::endl;

	auto max_iter=thrust::max_element(d1.begin(),d1.end());
	std::cout<<"max_element: "<<(*max_iter)<<std::endl;

	int inner_prod=thrust::inner_product(d1.begin(),d1.end(),d2.begin(),0);
	std::cout<<"inner_product: "<<inner_prod<<std::endl;

	////sort
	int a[8]={1,3,4,2,5,6,8,7};
	thrust::sort(a,a+8);
	std::cout<<"sort: ";for(const auto& x:a)std::cout<<x<<", ";std::cout<<std::endl;

	////constant iterator
	thrust::constant_iterator<int> c(10);
	thrust::transform(/*src1*/d1.begin(),d1.end(),/*src2*/c,/*des*/d2.begin(),my_func(4));
	Print(d2);
}

int main()
{
	Thrust_Vectors();
	
	return 0;
}