#include <cstdio>

__global__ void vec_set(int* x,int val)
{
	int i=threadIdx.x;
	x[i]=val;
}

__global__ void vec_add(int* a,int* b,int* c)
{
	int i=threadIdx.x;

	a[i]=b[i]+c[i];
	printf("a[%d]=%d\n",i,a[i]);
}

int main()
{
	int *a_dev,*b_dev,*c_dev;
	const int s=128;

	cudaMalloc((void**)&a_dev,s*sizeof(int));
	cudaMalloc((void**)&b_dev,s*sizeof(int));
	cudaMalloc((void**)&c_dev,s*sizeof(int));

	vec_set<<<1,s>>>(a_dev,0);
	vec_set<<<1,s>>>(b_dev,1);
	vec_set<<<1,s>>>(c_dev,2);

	vec_add<<<1,s>>>(a_dev,b_dev,c_dev);

	cudaFree(a_dev);
	cudaFree(b_dev);
	cudaFree(c_dev);

	return 0;
}